FROM ghcr.io/graalvm/graalvm-ce:java11-21.0.0.2 AS graalvm
VOLUME /tmp
ADD build/libs/financeapi-0.1-all.jar app.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]