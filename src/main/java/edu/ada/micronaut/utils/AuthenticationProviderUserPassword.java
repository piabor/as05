package edu.ada.micronaut.utils;

import edu.ada.micronaut.model.entity.UserEntity;
import edu.ada.micronaut.model.repository.UserRepository;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.http.HttpRequest;
import io.micronaut.security.authentication.*;
import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import org.reactivestreams.Publisher;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.Optional;

@Singleton
public class AuthenticationProviderUserPassword implements AuthenticationProvider {

    @Inject
    UserRepository userRepository;

    @Override
    public Publisher<AuthenticationResponse> authenticate(@Nullable HttpRequest<?> httpRequest, AuthenticationRequest<?, ?> authenticationRequest) {
        return Flowable.create(emitter -> {
            Optional<UserEntity> currentUser = userRepository.findByEmail(authenticationRequest.getIdentity().toString());
            if(currentUser.isPresent()) {
                String email = currentUser.get().getEmail();
                String password = currentUser.get().getPassword();
                if (authenticationRequest.getIdentity().equals(email) &&
                        PasswordEncryption.hashPassword(authenticationRequest.getSecret().toString()).equals(password)) {
                    emitter.onNext(new UserDetails((String) authenticationRequest.getIdentity(), new ArrayList<>()));
                    emitter.onComplete();
                } else {
                    emitter.onError(new AuthenticationException(new AuthenticationFailed()));
                }
            } else{
                emitter.onError(new AuthenticationException(new AuthenticationFailed()));
            }
        }, BackpressureStrategy.ERROR);
    }
}