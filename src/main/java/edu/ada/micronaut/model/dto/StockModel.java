package edu.ada.micronaut.model.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class StockModel implements Serializable {
    private String stockName;
    private String stockSymbol;
    private BigDecimal price;
    private String currency;
    private Long volume;

    public StockModel() {
    }

    public StockModel(String stockName, String stockSymbol, BigDecimal price, String currency, Long volume) {
        this.stockName = stockName;
        this.stockSymbol = stockSymbol;
        this.price = price;
        this.currency = currency;
        this.volume = volume;
    }

    public String getStockName() {
        return stockName;
    }

    public void setStockName(String stockName) {
        this.stockName = stockName;
    }

    public String getStockSymbol() {
        return stockSymbol;
    }

    public void setStockSymbol(String stockSymbol) {
        this.stockSymbol = stockSymbol;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public Long getVolume() {
        return volume;
    }

    public void setVolume(Long volume) {
        this.volume = volume;
    }

    @Override
    public String toString() {
        return "StockModel{" +
                "stockName='" + stockName + '\'' +
                ", stockSymbol='" + stockSymbol + '\'' +
                ", price=" + price +
                ", currency='" + currency + '\'' +
                ", volume=" + volume +
                '}';
    }
}
