package edu.ada.micronaut.service;

import edu.ada.micronaut.model.dto.StockModel;

import java.util.List;

public interface MarketStackService {
    List<StockModel> getStockData(String[] indices);
}
