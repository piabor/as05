package edu.ada.micronaut.service.impl;

import edu.ada.micronaut.model.dto.UserModel;
import edu.ada.micronaut.model.entity.UserEntity;
import edu.ada.micronaut.model.repository.UserRepository;
import edu.ada.micronaut.service.AuthenticationService;
import edu.ada.micronaut.utils.PasswordEncryption;
import edu.ada.micronaut.utils.StringValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Optional;

@Singleton
public class AuthenticationServiceImpl implements AuthenticationService {

    protected static final Logger logger = LoggerFactory.getLogger(AuthenticationServiceImpl.class);

    @Inject
    UserRepository userRepository;

    @Override
    public int registerUser(UserModel userModel) {
        try {
            Optional<UserEntity> userWithThisEmail = userRepository.findByEmail(userModel.getEmail());
            if(userWithThisEmail.isEmpty()) {
                if (StringValidator.isValidEmail(userModel.getEmail())) {
                    if (StringValidator.isValidPassword(userModel.getPassword())) {
                        if (StringValidator.isValidName(userModel.getFirstName()) && StringValidator.isValidName(userModel.getLastName())) {
                            userModel.setPassword(PasswordEncryption.hashPassword(userModel.getPassword()));
                            userRepository.save(new UserEntity(userModel));
                        } else {
                            return -3;
                        }
                    } else {
                        return -2;
                    }
                    return 1;
                } else {
                    return -1;
                }
            } else{
                return -4;
            }
        } catch (Exception e){
            logger.error(e.getMessage());
            return 0;
        }
    }
}
