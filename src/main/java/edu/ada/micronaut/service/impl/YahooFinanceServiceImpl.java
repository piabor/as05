package edu.ada.micronaut.service.impl;

import edu.ada.micronaut.model.dto.StockModel;
import edu.ada.micronaut.service.YahooFinanceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import yahoofinance.Stock;
import yahoofinance.YahooFinance;

import javax.inject.Singleton;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Singleton
public class YahooFinanceServiceImpl implements YahooFinanceService {

    protected static final Logger logger = LoggerFactory.getLogger(YahooFinanceServiceImpl.class);

    @Override
    public List<StockModel> getStockData(String[] indices) {
        List<StockModel> stockList = new ArrayList<>();
        try {
            for (String index : indices) {
                String stockIndex = index.toUpperCase(Locale.ROOT);
                Stock stock = YahooFinance.get(stockIndex);
                if (stock != null) {
                    StockModel stockModel = new StockModel();
                    stockModel.setStockName(stock.getName());
                    stockModel.setStockSymbol(stock.getSymbol());
                    stockModel.setPrice(stock.getQuote().getPrice());
                    stockModel.setCurrency(stock.getCurrency());
                    stockModel.setVolume(stock.getQuote().getVolume());
                    stockList.add(stockModel);
                } else{
                    logger.error("User requested data for invalid index - " + stockIndex);
                }
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        return stockList;
    }

}
