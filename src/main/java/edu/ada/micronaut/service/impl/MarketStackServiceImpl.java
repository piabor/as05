package edu.ada.micronaut.service.impl;

import com.nimbusds.jose.shaded.json.JSONArray;
import com.nimbusds.jose.shaded.json.JSONObject;
import com.nimbusds.jose.shaded.json.parser.JSONParser;
import com.nimbusds.jose.shaded.json.parser.ParseException;
import edu.ada.micronaut.model.dto.StockModel;
import edu.ada.micronaut.service.MarketStackService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

@Singleton
public class MarketStackServiceImpl implements MarketStackService {

    protected static final Logger logger = LoggerFactory.getLogger(MarketStackServiceImpl.class);

    @Override
    public List<StockModel> getStockData(String[] indices) {
        List<StockModel> stockList = new ArrayList<>();
        HttpURLConnection conn = null;
        try {
            for (String index : indices) {
                String stockIndex = index.toUpperCase(Locale.ROOT);
                URL url = new URL("http://api.marketstack.com/v1/eod/latest?access_key=8ce80f413db6909a1ad3efa7f72aec6d&symbols=" + stockIndex);
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.connect();
                int responseCode = conn.getResponseCode();
                if (responseCode == 200) {
                    StockModel stockModel = new StockModel();
                    StringBuilder inline = new StringBuilder();
                    Scanner scanner = new Scanner(url.openStream());
                    while (scanner.hasNext()) {
                        inline.append(scanner.nextLine());
                    }
                    scanner.close();
                    JSONParser parse = new JSONParser();
                    JSONObject data_obj = (JSONObject) parse.parse(inline.toString());
                    JSONArray obj = (JSONArray) data_obj.get("data");
                    if (obj != null) {
                        JSONObject latestData = (JSONObject) obj.get(0);
                        stockModel.setStockSymbol(latestData.get("symbol").toString());
                        stockModel.setPrice(new BigDecimal(latestData.get("close").toString()));
                        stockModel.setCurrency("USD");
                        stockModel.setVolume((long) Double.parseDouble(latestData.get("volume").toString()));
                        stockList.add(stockModel);
                    }
                } else {
                    StringBuilder inline = new StringBuilder();
                    Scanner scanner = new Scanner(conn.getErrorStream());
                    while (scanner.hasNext()) {
                        inline.append(scanner.nextLine());
                    }
                    scanner.close();
                    JSONParser parse = new JSONParser();
                    JSONObject data_obj = (JSONObject) parse.parse(inline.toString());
                    JSONObject errorObj = (JSONObject) data_obj.get("error");
                    if (errorObj != null && !errorObj.isEmpty()) {
                        logger.error(errorObj.get("message").toString() + " - " + stockIndex);
                    }
                }
                conn.disconnect();
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return stockList;
    }
}
