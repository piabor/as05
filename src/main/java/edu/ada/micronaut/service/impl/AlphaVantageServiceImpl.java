package edu.ada.micronaut.service.impl;

import com.nimbusds.jose.shaded.json.JSONObject;
import com.nimbusds.jose.shaded.json.parser.JSONParser;
import com.nimbusds.jose.shaded.json.parser.ParseException;
import edu.ada.micronaut.model.dto.StockModel;
import edu.ada.micronaut.service.AlphaVantageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

@Singleton
public class AlphaVantageServiceImpl implements AlphaVantageService {

    protected static final Logger logger = LoggerFactory.getLogger(AlphaVantageServiceImpl.class);

    @Override
    public List<StockModel> getStockData(String[] indices){
        List<StockModel> stockList = new ArrayList<>();
        HttpURLConnection conn = null;
        try {
            for (String index : indices) {
                String stockIndex = index.toUpperCase(Locale.ROOT);
                URL url = new URL("https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=" + stockIndex + "&apikey=QJTGWFL5VF91DSV7");
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.connect();
                StockModel stockModel = new StockModel();
                StringBuilder inline = new StringBuilder();
                Scanner scanner = new Scanner(url.openStream());
                while (scanner.hasNext()) {
                    inline.append(scanner.nextLine());
                }
                scanner.close();
                    JSONParser parse = new JSONParser();
                    JSONObject data_obj = (JSONObject) parse.parse(inline.toString());
                    JSONObject obj = (JSONObject) data_obj.get("Global Quote");
                    if (obj != null && !obj.isEmpty()) {
                        stockModel.setStockSymbol(obj.get("01. symbol").toString());
                        stockModel.setPrice(new BigDecimal(obj.get("05. price").toString()));
                        stockModel.setCurrency("USD");
                        stockModel.setVolume((long) Double.parseDouble(obj.get("06. volume").toString()));
                        stockList.add(stockModel);
                    } else {
                        Object messageObj = data_obj.get("Note");
                        if (messageObj != null) {
                            String message = messageObj.toString();
                            logger.error(message);
                            stockModel.setStockName(message);
                            stockList.add(stockModel);
                            conn.disconnect();
                            break;
                        } else{
                            logger.error("User requested data for invalid index - " + stockIndex);
                        }

                }
                conn.disconnect();
            }
        } catch (IOException | ParseException e) {
            logger.error(e.getMessage());
        }
        return stockList;
    }
}
