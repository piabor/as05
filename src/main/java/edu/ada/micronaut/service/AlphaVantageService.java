package edu.ada.micronaut.service;

import edu.ada.micronaut.model.dto.StockModel;

import java.util.List;

public interface AlphaVantageService {
    List<StockModel> getStockData(String[] indices);
}
