package edu.ada.micronaut.service;

import edu.ada.micronaut.model.dto.UserModel;

public interface AuthenticationService {
    int registerUser (UserModel userModel);
}
