package edu.ada.micronaut.controller.impl;

import edu.ada.micronaut.controller.FinanceController;
import edu.ada.micronaut.model.dto.StockModel;
import edu.ada.micronaut.service.AlphaVantageService;
import edu.ada.micronaut.service.MarketStackService;
import edu.ada.micronaut.service.YahooFinanceService;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.security.utils.SecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.inject.Inject;
import java.util.List;
import java.util.Locale;

@Controller("/finance")
@Secured(SecurityRule.IS_AUTHENTICATED)
public class FinanceControllerImpl implements FinanceController {

    protected static final Logger logger = LoggerFactory.getLogger(FinanceControllerImpl.class);

    @Inject
    private YahooFinanceService yahooFinanceService;

    @Inject
    private MarketStackService marketStackService;

    @Inject
    private AlphaVantageService alphaVantageService;

    @Inject
    private SecurityService securityService;

    @Override
    @Get
    @Produces(MediaType.APPLICATION_JSON)
    public HttpResponse getFinancialData(
            @QueryValue("provider") String dataProviderName,
            @QueryValue("stock-index") String stockIndex
    ) {
        String[] indices = stockIndex.split(",");
        if(dataProviderName.toLowerCase(Locale.ROOT).equals("yahoo")){
            List<StockModel> result = yahooFinanceService.getStockData(indices);
            if(result.size() > 0) {
                logger.info("Data is provided to " + securityService.username().get() + " by Yahoo Finance");
                return HttpResponse.ok(result);
            } else {
                return HttpResponse.notFound("No result has been found for the entered indices.");
            }
        } else if(dataProviderName.toLowerCase(Locale.ROOT).equals("marketstack")){
            List<StockModel> result = marketStackService.getStockData(indices);
            if(result.size() > 0) {
                logger.info("Data is provided to " + securityService.username().get() + " by MarketStack");
                return HttpResponse.ok(result);
            } else {
                return HttpResponse.notFound("No result has been found for the entered indices.");
            }
        }
        else if(dataProviderName.toLowerCase(Locale.ROOT).equals("alphavantage")){
            List<StockModel> result = alphaVantageService.getStockData(indices);
            if(result.size() > 0) {
                logger.info("Data is provided to " + securityService.username().get() + " by Alpha Vantage");
                return HttpResponse.ok(result);
            } else {
                return HttpResponse.notFound("No result has been found for the entered indices.");
            }
        } else{
            return HttpResponse.badRequest("Provider name is invalid.");
        }
    }
}
