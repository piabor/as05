package edu.ada.micronaut.controller.impl;

import edu.ada.micronaut.controller.AuthenticationWS;
import edu.ada.micronaut.model.dto.UserModel;
import edu.ada.micronaut.service.AuthenticationService;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.rules.SecurityRule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
@Controller("/")
@Secured(SecurityRule.IS_ANONYMOUS)
public class AuthenticationWSImpl implements AuthenticationWS {

    protected static final Logger logger = LoggerFactory.getLogger(AuthenticationWSImpl.class);

    @Inject
    private AuthenticationService authenticationService;

    @Override
    @Post("/register")
    @Produces(MediaType.TEXT_PLAIN)
    public HttpResponse register(
            @Body UserModel formData
    ){
        int result = authenticationService.registerUser(formData);
        if(result == -2){
            logger.warn("Password validation failed while registering");
            return HttpResponse.badRequest("Passwords should be 8-20 long and contain at least one number, one upper case and one lower case letter.");
        } else if (result == -1){
            logger.warn("Invalid email address is given");
            return HttpResponse.badRequest("Invalid email address!");
        } else if (result == -3){
            logger.warn("Non-text or blank input is given for first name and/or last name");
            return HttpResponse.badRequest("You cannot leave first and last name field blank and cannot enter symbols or numbers.");
        } else if (result == -4){
            logger.error("A used email address is given");
            return HttpResponse.badRequest("This email address is already being used.");
        } else{
            logger.info("A user has been registered");
            return HttpResponse.created("You have been successfully registered!");
        }
    }
}
