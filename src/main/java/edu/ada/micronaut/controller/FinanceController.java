package edu.ada.micronaut.controller;

import io.micronaut.http.HttpResponse;

public interface FinanceController {
    HttpResponse getFinancialData(String dataProviderName, String stockIndex);
}
