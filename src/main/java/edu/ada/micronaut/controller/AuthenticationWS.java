package edu.ada.micronaut.controller;

import edu.ada.micronaut.model.dto.UserModel;
import io.micronaut.http.HttpResponse;

public interface AuthenticationWS {
    HttpResponse register(UserModel formData);
}
